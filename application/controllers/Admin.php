<?php
class admin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin_model');
    }

    public function login()
    {

        if ($this->session->admindata('optvgloble_admin_id')) {
            redirect('admin/');
        }

        $data['title'] = 'Nexco Japan';

        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() === FALSE) {

            $this->load->view('templates/admin/header.php');
            $this->load->view('templates/admin/login.php', $data);
        } else {

            $username = $this->input->post('username');
            $password = $this->input->post('password');

            $optvgloble_admin_id = $this->admin_model->login($username, $password);

            if ($optvgloble_admin_id) {
                $admin_data = array(
                    'optvgloble_admin_id' => $optvgloble_admin_id,
                    'japan_email' => $username,
                    'submit_alogged_in' => true
                );
                $this->session->set_admindata($admin_data);

                redirect('admin/');
            } else {

                $this->session->set_flashdata('login_failed', 'Login is invalid. Incorrect username or password.');
                redirect('admin/login');
            }
        }
    }

    public function logout()
    {

        $this->session->unset_userdata('optvgloble_admin_id');
        $this->session->unset_userdata('japan_email');
        $this->session->unset_userdata('submit_alogged_in');

        $this->session->set_flashdata('admin_loggedout', 'You are now logged out');
        redirect('admin/login');
    }

    function do_upload()
    {

        $config = array(
            'upload_path' => "assets/uploads/",
            'allowed_types' => "gif|jpg|png|jpeg|pdf",
            'overwrite' => false,
            'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
            'max_height' => "5000",
            'max_width' => "5000"
        );

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('userfile')) {
            $imgdata = array('upload_data' => $this->upload->data());

            $imgname = $imgdata['upload_data']['file_name'];
        } else {
            $error = array('error' => $this->upload->display_errors());
            echo '<pre>';
            print_r($error);
            echo '<pre>';
            exit;
        }

        return $imgname;
    }

    public function index()
    {
        if (!$this->session->admindata('optvgloble_admin_id')) {
            redirect('admin/login');
        }

        $data['live'] = $this->admin_model->get_live_streaming();
        $data['newses'] = $this->admin_model->get_news_limit();



        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/dashboard.php', $data);
        $this->load->view('templates/admin/footer.php');
    }


    // staff Order Manager Start


    public function addstaff()
    {
        if (!$this->session->admindata('optvgloble_admin_id')) {
            redirect('admin/login');
        }

        $this->form_validation->set_rules('username', 'username', 'required');


        if ($this->form_validation->run() === FALSE) {


            $this->load->view('templates/admin/header.php');
            $this->load->view('templates/admin/navbar.php');
            $this->load->view('templates/admin/aside.php');
            $this->load->view('templates/admin/addstaff.php');
            $this->load->view('templates/admin/footer.php');
        } else {
            $enc_password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);

            $imgname = $this->do_upload();

            $this->admin_model->addstaff($imgname, $enc_password);

            redirect('admin/addstaff');
        }
    }

    public function managestaff()
    {
        if (!$this->session->admindata('optvgloble_admin_id')) {
            redirect('admin/login');
        }

        $this->form_validation->set_rules('username', 'username', 'required');

        if ($this->form_validation->run() === FALSE) {


            $data['staffs'] = $this->admin_model->get_staff();

            $this->load->view('templates/admin/header.php');
            $this->load->view('templates/admin/navbar.php');
            $this->load->view('templates/admin/aside.php');
            $this->load->view('templates/admin/managestaff.php', $data);
            $this->load->view('templates/admin/footer.php');
        } else {
            if (!file_exists($_FILES["userfile"]["tmp_name"])) {
                $staffid = $this->input->post('staffid');
                $currUser = $this->admin_model->get_ajax_staff($staffid);

                $imgname = $currUser["image"];
                $this->admin_model->update_staff($imgname,  $staffid);
            } else {
                $imgname = $this->do_upload();
                $productid = $this->input->post('staffid');
                $this->admin_model->update_staff($imgname,  $staffid);
            }

            redirect('admin/managestaff');
        }
    }

    public function staffdelete($staffid)
    {

        $this->admin_model->del_staff($staffid);
        redirect('admin/managestaff');
    }

    // staff Order Manager End



    // news Order Manager Start


    public function news()
    {
        if (!$this->session->admindata('optvgloble_admin_id')) {
            redirect('admin/login');
        }
        $data['newses'] = $this->admin_model->get_news();

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/news.php', $data);
        $this->load->view('templates/admin/footer.php');
    }

    public function addnews()
    {
        if (!$this->session->admindata('optvgloble_admin_id')) {
            redirect('admin/login');
        }

        $this->form_validation->set_rules('newstitle', 'newstitle', 'required');


        if ($this->form_validation->run() === FALSE) {


            redirect('admin/news?failRegistration');
        } else {
            $imgname = $this->do_upload();
            $this->admin_model->addnews($imgname);

            redirect('admin/news');
        }
    }

    public function updatenews()
    {
        if (!$this->session->admindata('optvgloble_admin_id')) {
            redirect('admin/login');
        }

        $this->form_validation->set_rules('newstitle', 'newstitle', 'required');

        if ($this->form_validation->run() === FALSE) { } else {
            if (!file_exists($_FILES["userfile"]["tmp_name"])) {
                $newsid = $this->input->post('newsid');
                $images = $this->admin_model->get_ajax_news($newsid);

                $imgname = $images["image"];
                $this->admin_model->update_news($imgname, $newsid);
            } else {
                $imgname = $this->do_upload();
                $newsid = $this->input->post('newsid');
                $this->admin_model->update_news($imgname, $newsid);
            }

            redirect('admin/news');
        }
    }


    public function disable($newsid)
    {

        $this->admin_model->disable($newsid);
        redirect('admin/news');
    }

    public function enable($newsid)
    {

        $this->admin_model->enable($newsid);
        redirect('admin/news');
    }

    // news Order Manager End

    // tvshow Order Manager Start


    public function tvshow()
    {
        if (!$this->session->admindata('optvgloble_admin_id')) {
            redirect('admin/login');
        }
        $data['tvshowes'] = $this->admin_model->get_tvshow();

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/tvshow.php', $data);
        $this->load->view('templates/admin/footer.php');
    }

    public function addtvshow()
    {
        if (!$this->session->admindata('optvgloble_admin_id')) {
            redirect('admin/login');
        }

        $this->form_validation->set_rules('tvshowtitle', 'tvshowtitle', 'required');


        if ($this->form_validation->run() === FALSE) {


            redirect('admin/tvshow?failRegistration');
        } else {
            $imgname = $this->do_upload();
            $this->admin_model->addtvshow($imgname);

            redirect('admin/tvshow');
        }
    }

    public function updatetvshow()
    {
        if (!$this->session->admindata('optvgloble_admin_id')) {
            redirect('admin/login');
        }

        $this->form_validation->set_rules('tvshowtitle', 'tvshowtitle', 'required');

        if ($this->form_validation->run() === FALSE) { } else {
            if (!file_exists($_FILES["userfile"]["tmp_name"])) {
                $tvshowid = $this->input->post('tvshowid');
                $images = $this->admin_model->get_ajax_tvshow($tvshowid);

                $imgname = $images["image"];
                $this->admin_model->update_tvshow($imgname, $tvshowid);
            } else {
                $imgname = $this->do_upload();
                $tvshowid = $this->input->post('tvshowid');
                $this->admin_model->update_tvshow($imgname, $tvshowid);
            }

            redirect('admin/tvshow');
        }
    }


    public function disabletvshow($tvshowid)
    {

        $this->admin_model->disabletvshow($tvshowid);
        redirect('admin/tvshow');
    }

    public function enabletvshow($tvshowid)
    {

        $this->admin_model->enabletvshow($tvshowid);
        redirect('admin/tvshow');
    }

    // tvshow Order Manager End

    public function offline($liveid)
    {

        $this->admin_model->offline($liveid);
        redirect('admin/livepage');
    }

    public function online($liveid)
    {

        $this->admin_model->online($liveid);
        redirect('admin/livepage');
    }




    public function livepage()
    {
        if (!$this->session->admindata('optvgloble_admin_id')) {
            redirect('admin/login');
        }

        $data['live'] = $this->admin_model->get_live_streaming();

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/livepage.php', $data);
        $this->load->view('templates/admin/footer.php');
    }

    public function addlivepage()
    {
        if (!$this->session->admindata('optvgloble_admin_id')) {
            redirect('admin/login');
        }


        if (!file_exists($_FILES["userfile"]["tmp_name"])) {
            $livepageid = '1';
            $livepag = $this->admin_model->get_live($livepageid);

            $imgname = $livepag["thumnail"];
            $this->admin_model->addlivepage($imgname,  $livepageid);
        } else {
            $imgname = $this->do_upload();
            $livepageid = '1';

            $this->admin_model->addlivepage($imgname, $livepageid);

            redirect('admin/livepage');
        }
    }







    public function blogs()
    {
        if (!$this->session->admindata('optvgloble_admin_id')) {
            redirect('admin/login');
        }

        $data['blogses'] = $this->admin_model->get_blogs();

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/blogs.php', $data);
        $this->load->view('templates/admin/footer.php');
    }

    public function addblogs()
    {
        if (!$this->session->admindata('optvgloble_admin_id')) {
            redirect('admin/login');
        }

        $this->form_validation->set_rules('blogs', 'blogs', 'required');


        if ($this->form_validation->run() === FALSE) { } else {

            $imgname = $this->do_upload();
            $this->admin_model->addblogs($imgname);

            redirect('admin/blogs');
        }
    }



    public function updateblogs()
    {
        if (!$this->session->admindata('optvgloble_admin_id')) {
            redirect('admin/login');
        }
        $this->form_validation->set_rules('blogs', 'blogs', 'required');

        if ($this->form_validation->run() === FALSE) { } else {
            if (!file_exists($_FILES["userfile"]["tmp_name"])) {
                $blogsid = $this->input->post('blogsid');
                $blogs = $this->admin_model->get_ajax_blogs($blogsid);

                $imgname = $blogs["blogsimg"];
                $this->admin_model->updateblogs($imgname,  $blogsid);
            } else {
                $imgname = $this->do_upload();
                $blogsid = $this->input->post('blogsid');
                $this->admin_model->updateblogs($imgname,  $blogsid);
            }
        }
        redirect('admin/blogs');
    }

    public function deleteblogs($blogsid)
    {

        $this->admin_model->deleteblogs($blogsid);
        redirect('admin/blogs');
    }

    public function aboutus()
    {
        if (!$this->session->admindata('optvgloble_admin_id')) {
            redirect('admin/login');
        }

        $data['abouts'] = $this->admin_model->get_about();

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/aboutus.php', $data);
        $this->load->view('templates/admin/footer.php');
    }


    public function update_about()
    {
        if (!$this->session->admindata('optvgloble_admin_id')) {
            redirect('admin/login');
        }
        $this->admin_model->update_about();

        redirect('admin/aboutus');
    }

    public function settings()
    {
        if (!$this->session->admindata('optvgloble_admin_id')) {
            redirect('admin/login');
        }

        $data['imges'] = $this->admin_model->img();

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/settings.php', $data);
        $this->load->view('templates/admin/footer.php');
    }

    public function updateimage01()
    {
        if (!$this->session->admindata('optvgloble_admin_id')) {
            redirect('admin/login');
        }
        $imgname = $this->do_upload();
        $this->admin_model->updateimage01($imgname);

        redirect('admin/settings');
    }
    public function updateimage02()
    {
        if (!$this->session->admindata('optvgloble_admin_id')) {
            redirect('admin/login');
        }
        $imgname = $this->do_upload();

        $this->admin_model->updateimage02($imgname);

        redirect('admin/settings');
    }
    public function updateimage03()
    {
        if (!$this->session->admindata('optvgloble_admin_id')) {
            redirect('admin/login');
        }
        $imgname = $this->do_upload();

        $this->admin_model->updateimage03($imgname);

        redirect('admin/settings');
    }

    //////////////////////////////////////////////Ajax//////////////////////////////////

    public function ajax_edit_staff_adminmodal($staffid)
    {

        $data['staff'] = $this->admin_model->get_ajax_staff($staffid);

        $this->load->view('templates/ajax/editstaff.php', $data);
    }

    public function ajax_edit_news_adminmodal($newsid)
    {

        $data['news'] = $this->admin_model->get_ajax_news($newsid);

        $this->load->view('templates/ajax/editnews.php', $data);
    }

    public function ajax_edit_tvshow_adminmodal($tvshowid)
    {

        $data['tvshow'] = $this->admin_model->get_ajax_tvshow($tvshowid);

        $this->load->view('templates/ajax/edittvshow.php', $data);
    }

    public function ajax_edit_blogs_adminmodal($blogsid)
    {

        $data['blogs'] = $this->admin_model->get_ajax_blogs($blogsid);

        $this->load->view('templates/ajax/editblogs.php', $data);
    }

    public function ajax_view_blogs_adminmodal($blogsid)
    {

        $data['blogs'] = $this->admin_model->get_ajax_blogs($blogsid);

        $this->load->view('templates/ajax/viewblogs.php', $data);
    }



    /////////////////////////////////api


    public function live_streaming_api()
    {

        $data['live'] = $this->admin_model->get_live_streaming_api();



        echo json_encode($data);
    }

    public function news_api()
    {

        $data['news'] = $this->admin_model->get_news_api();

        echo json_encode($data);
    }
    public function tvshow_api()
    {

        $data['tvshow'] = $this->admin_model->get_tvshow_api();

        echo json_encode($data);
    }

    public function news_api_8()
    {

        $data['news'] = $this->admin_model->get_news_api_limit();

        echo json_encode($data);
    }


    public function aboutus_api()
    {

        $data['aboutus'] = $this->admin_model->get_aboutus_api();

        echo json_encode($data);
    }

    public function blogs_api()
    {

        $data['blogs'] = $this->admin_model->get_blogs();

        echo json_encode($data);
    }

    public function slider()
    {

        $data['sliders'] = $this->admin_model->get_slider();

        echo json_encode($data);
    }
}
