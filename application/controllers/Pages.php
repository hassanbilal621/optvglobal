<?php
class Pages extends CI_Controller{

    public function __construct()
    {
      parent::__construct();
      $this->load->model('blog_model');
      $this->load->model('pages_model');
    }
	
	/*
     * Function: view
     * Purpose: This controller is responsible for showing static pages,
     * Params:  $page: optional parameter, default is home. This is the name of the page to view
     * Return: none
     */
    public function view($page='home',$offset=0){


        if($this->session->userdata('submitmypaper_id')){
            redirect('users/');
        }

        $data['academic_levels'] = $this->pages_model->get_academic_level(); 
        $data['noofdays'] = $this->pages_model->get_noofdays(); 
        $data['noofpages'] = $this->pages_model->get_noofpages(); 
        $data['typesofpapers'] = $this->pages_model->get_typesofpaper(); 

        // echo '<pre>';
        // print_r($data);
        // echo '<pre>';
        // die;

        
        $this->load->view('templates/indexn/header');
        $this->load->view('templates/indexn/navbar', $data);
        $this->load->view('templates/indexn/index', $data);
        $this->load->view('templates/indexn/footer');
    }

    public function howitworks()
    {
        if($this->session->userdata('submitmypaper_id')){
            redirect('users/');
        }

        $data['academic_levels'] = $this->pages_model->get_academic_level(); 
        // echo $this->router->fetch_class();
        // echo $this->router->fetch_method();
        // die;
        // echo '<pre>';
        // print_r($data);
        // echo '<pre>';
        // die;

        $this->load->view('templates/indexn/header');
        $this->load->view('templates/indexn/navbar', $data);
        $this->load->view('templates/indexn/howitwork', $data);
        $this->load->view('templates/indexn/footer');

    }


    public function pricing()
    {
        if($this->session->userdata('submitmypaper_id')){
            redirect('users/');
        }

        $data['academic_levels'] = $this->pages_model->get_academic_level(); 
        // echo $this->router->fetch_class();
        // echo $this->router->fetch_method();
        // die;
        // echo '<pre>';
        // print_r($data);
        // echo '<pre>';
        // die;

        $this->load->view('templates/indexn/header');
        $this->load->view('templates/indexn/navbar', $data);
        $this->load->view('templates/indexn/prices', $data);
        $this->load->view('templates/indexn/footer');

    }



    public function testimonials()
    {
        if($this->session->userdata('submitmypaper_id')){
            redirect('users/');
        }

        $data['academic_levels'] = $this->pages_model->get_academic_level(); 
        // echo $this->router->fetch_class();
        // echo $this->router->fetch_method();
        // die;
        // echo '<pre>';
        // print_r($data);
        // echo '<pre>';
        // die;

        $this->load->view('templates/indexn/header');
        $this->load->view('templates/indexn/navbar', $data);
        $this->load->view('templates/indexn/testimonial', $data);
        $this->load->view('templates/indexn/footer');

    }


    public function faq()
    {
        if($this->session->userdata('submitmypaper_id')){
            redirect('users/');
        }

        $data['academic_levels'] = $this->pages_model->get_academic_level(); 
        // echo $this->router->fetch_class();
        // echo $this->router->fetch_method();
        // die;
        // echo '<pre>';
        // print_r($data);
        // echo '<pre>';
        // die;

        $this->load->view('templates/indexn/header');
        $this->load->view('templates/indexn/navbar', $data);
        $this->load->view('templates/indexn/faq', $data);
        $this->load->view('templates/indexn/footer');

    }

    public function samples()
    {
        if($this->session->userdata('submitmypaper_id')){
            redirect('users/');
        }

        $data['academic_levels'] = $this->pages_model->get_academic_level(); 
        // echo $this->router->fetch_class();
        // echo $this->router->fetch_method();
        // die;
        // echo '<pre>';
        // print_r($data);
        // echo '<pre>';
        // die;

        $this->load->view('templates/indexn/header');
        $this->load->view('templates/indexn/navbar', $data);
        $this->load->view('templates/indexn/sample', $data);
        $this->load->view('templates/indexn/footer');

    }


    public function login()
    {
        if($this->session->userdata('submit_user_id')){
            redirect('users/');
        }

        $data['academic_levels'] = $this->pages_model->get_academic_level(); 
        // echo $this->router->fetch_class();
        // echo $this->router->fetch_method();
        // die;
        // echo '<pre>';
        // print_r($data);
        // echo '<pre>';
        // die;

        $this->load->view('templates/indexn/header');
        $this->load->view('templates/indexn/navbar', $data);
        $this->load->view('templates/indexn/login', $data);
        $this->load->view('templates/indexn/footer');

    }


    public function signup()
    {
        if($this->session->userdata('submit_user_id')){
            redirect('users/');
        }

        $data['academic_levels'] = $this->pages_model->get_academic_level(); 
        // echo $this->router->fetch_class();
        // echo $this->router->fetch_method();
        // die;
        // echo '<pre>';
        // print_r($data);
        // echo '<pre>';
        // die;

        $this->load->view('templates/indexn/header');
        $this->load->view('templates/indexn/navbar', $data);
        $this->load->view('templates/indexn/signup', $data);
        $this->load->view('templates/indexn/footer');

    }

    public function order()
    {


        $data['academic_levels'] = $this->pages_model->get_academic_level(); 
        $data['noofdays'] = $this->pages_model->get_noofdays(); 
        $data['noofpages'] = $this->pages_model->get_noofpages(); 
        $data['typesofpapers'] = $this->pages_model->get_typesofpaper(); 

        if($this->session->userdata('submitmypaper_id')){

            $userid= $this->session->userdata('submit_user_id');
            $currUser = $this->user_model->get_userinfo($userid);
       
        }

        $this->load->view('templates/indexn/header.php');
        // $this->load->view('templates/indexn/navbar.php', $data);
        $this->load->view('templates/index/order.php', $data);
        $this->load->view('templates/indexn/footer.php');
    

    }



    public function courses($categoryid)
    {
        $data['courses_categories'] = $this->blog_model->getcoursecategories();
        $data['single_course_category'] = $this->blog_model->single_course_category($categoryid); 
        $data['blog_categories'] = $this->blog_model->getblogcategory();
        $data['get_allcourses'] = $this->blog_model->getallcourses(); 
        // echo $this->router->fetch_class();
        // echo $this->router->fetch_method();
        // die;
        // echo '<pre>';
        // print_r($data);
        // echo '<pre>';
        // die;


        
        if(!$this->session->userdata('user_id'))
        {
            $this->load->view('templates/index/header.php');
            $this->load->view('templates/index/navbar.php', $data);
            $this->load->view('templates/index/courses.php', $data);
            $this->load->view('templates/index/footer.php');
        }

    }
    public function events($eventtypes)
    {

        $data['courses_categories'] = $this->blog_model->getcoursecategories(); 
        $data['blog_categories'] = $this->blog_model->getblogcategory(); 
        $data['events'] = $this->blog_model->getevents(); 
        

   
        $this->load->view('templates/index/header.php');
        $this->load->view('templates/index/navbar.php', $data);
        $this->load->view('templates/index/events.php', $data);
        $this->load->view('templates/index/footer.php');
    

    }


    public function aboutus($eventtypes)
    {

        $data['courses_categories'] = $this->blog_model->getcoursecategories(); 
        $data['blog_categories'] = $this->blog_model->getblogcategory(); 

        if(!$this->session->userdata('user_id'))
        {
            $this->load->view('templates/index/header.php');
            $this->load->view('templates/index/navbar.php', $data);
            $this->load->view('templates/index/courses.php', $data);
            $this->load->view('templates/index/footer.php');
        }

    }

    public function contactus()
    {
        $data['courses_categories'] = $this->blog_model->getcoursecategories(); 
        $data['blog_categories'] = $this->blog_model->getblogcategory(); 
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');
        $this->form_validation->set_rules('subject', 'Subject', 'required');
        $this->form_validation->set_rules('comments', 'Comments', 'required');

        if($this->form_validation->run() === FALSE){

            $this->load->view('templates/index/header.php');
            $this->load->view('templates/index/navbar.php', $data);
            $this->load->view('templates/index/contact.php', $data);
            $this->load->view('templates/index/footer.php');
        }


        else{
			//For emailing reset password link
			$this->load->library('email');
            
            $this->email->from('support@marketthinktank.com', 'Support Market Think Tank');
            $this->email->to('info@marketthinktank.com',$this->input->post('name'));
            $this->email->subject($this->input->post('subject'));
            $this->email->message($this->input->post('comments'));
            
            $this->email->send();

            $this->session->set_flashdata('email_sent', 'Your message has been sent sucessfully.');


            $this->load->view('templates/index/header.php');
            $this->load->view('templates/index/navbar.php', $data);
            $this->load->view('templates/index/contact.php', $data);
            $this->load->view('templates/index/footer.php');
            
        }

  

    }
    public function getcountry()
	{
		$data['countries'] = $this->jobs_model->getcountry();

		$this->load->view('templates/ajax/getcountry.php', $data);
	}

	public function getcity()
	{
		
		$data['cities'] = $this->jobs_model->getcity();

		$this->load->view('templates/ajax/getcity.php', $data);
	}

    
    public function services()
    {
        if(!$this->session->userdata('user_id'))
        {
            $this->load->view('templates/home/header.php');
            $this->load->view('templates/auth/navbar.php');
            $this->load->view('templates/services/services.php');
            $this->load->view('templates/home/footer.php');
        }
        else{
        $userid= $this->session->userdata('user_id');
        $currUser = $this->user_model->get_userinfo($userid);

        $data['user'] = $currUser;
        $data['title'] = "User Account";

        $this->load->view('templates/home/header.php');
        $this->load->view('templates/home/navbar.php');
        $this->load->view('templates/services/servicesuser', $data);
        $this->load->view('templates/home/footer.php');
        }

    }


	/*
     * Function: contact
     * Purpose: This controller is responsible for showing the contact page, 
				and processing the message sent 
				URL is /contact
     * Params:  $page: optional parameter, default is home. This is the name of the page to view
     * Return: none
     */
    public function contact()
    {
        $data['title'] = "Contact";
        
        $this->form_validation->set_rules('name','Name', 'required');
        $this->form_validation->set_rules('email','Email',  'required|valid_email');
        $this->form_validation->set_rules('body', 'Body', 'required');
        
        if($this->form_validation->run()==FALSE)
        {

            if(!$this->session->userdata('user_id'))
            {
                $this->load->view('templates/home/header.php');
                $this->load->view('templates/auth/navbar.php');
                $this->load->view('templates/pages/contact', $data);
                $this->load->view('templates/home/footer.php');
            }
            else{
            $userid= $this->session->userdata('user_id');
            $currUser = $this->user_model->get_userinfo($userid);
    
            $data['user'] = $currUser;
            $data['title'] = "User Account";
    
            $this->load->view('templates/home/header.php');
            $this->load->view('templates/home/navbar.php');
            $this->load->view('templates/pages/contact', $data);
            $this->load->view('templates/home/footer.php');
            }
        }
        else
        {
			/*
            $this->load->library('email');
            
            $this->email->from('abc@abc.com', 'NAME');
            $this->email->to($this->input->post('email'),$this->input->post('name'));
            $this->email->subject('Email from ciblog');
            $this->email->message($this->input->post('body'));
            
            $this->email->send();
            
            //$this->email->send(FALSE);
            //$this->email->print_debugger(array('headers'));
            */
         
            $this->session->set_flashdata('msg_sent','Your message has been sent');
            redirect('contact');
        }
        
    }
}
