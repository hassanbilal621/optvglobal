<?php
class Users extends CI_Controller{

	public function __construct()
    {
      parent::__construct();
	  $this->load->model('user_model');
    }



      
	public function login(){
		if($this->session->userdata('medical_user_id')){
            redirect('users/');
        }
		
        $data['title'] = 'Nexco Japan = Login';
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if($this->form_validation->run() === FALSE){
            
            $this->load->view('templates/users/header.php');
            $this->load->view('templates/users/login.php', $data);
            $this->load->view('templates/users/footer.php');


        } 
        else {
            $username = $this->input->post('username');
			$password = $this->input->post('password');

			$user_id = $this->user_model->login($username, $password);
			//$employee_id = $this->employee_model->login($username, $password);

			if($user_id){
                $user_data = array(
                    'medical_user_id' => $user_id,
                    'username' => $username,
                    'necxo_logged_in' => true
                );
                $this->session->set_userdata($user_data);
				
				
				//set cookie for 1 year
				$cookie = array(
					'name'   => 'medical_user_id',
					'value'  => $user_id,
					'expire' => time()+31556926
				);
				$this->input->set_cookie($cookie);

				
                $this->session->set_flashdata('user_loggedin', 'You are now logged in');
                redirect('users/');
			} 
	
            else {

                $this->session->set_flashdata('login_failed', 'Login is invalid. Incorrect username or password.');
                redirect('users/login');
            }		
        }
	}
    
	
	public function index()
	{
		// if(!$this->session->userdata('medical_user_id'))
		// {
		// 	redirect('users/login');
		// }
		$userid= $this->session->userdata('medical_user_id');
		$currUser = $this->user_model->get_userinfo($userid);

		$data['user'] = $currUser;
		$data['title'] = "User Account";

		$this->load->view('templates/users/header.php');
        $this->load->view('templates/users/navbar.php', $data);
        $this->load->view('templates/users/aside.php', $data);
        $this->load->view('templates/users/index.php', $data);
        $this->load->view('templates/users/footer.php');
	}


}
