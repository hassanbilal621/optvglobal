<div class="row">
	<div class="col s12">
		<div class="card">
			<div class="card-content">
				<h4 class="card-title">Edit Staff</h4>
				<?php echo form_open_multipart('admin/managestaff'); ?>
				<div class="row">
					<div class="col s6">
						<div class="input-field col s12">
							<div class="row">
								<div class="col s12">
									<label for="name2">First Name</label>
								</div>
								<div class="col s12">
                           <input id="name2" type="text" name="firstname" value="<?php echo $staff['firstname']; ?>">
                           <input type="hidden" name="staffid" value="<?php echo $staff['staff_id']; ?>">
								</div>
							</div>
						</div>
						<div class="input-field col s12">
							<div class="row">
								<div class="col s12">
									<label for="name2">Last Name</label>
								</div>
								<div class="col s12">
									<input id="name2" type="text" name="lastname" value="<?php echo $staff['lastname']; ?>">
								</div>
							</div>
						</div>
					</div>
					<div class="col s6">
						<div class="input-field col s12">
							<div id="file-upload" class="section">
								<div class="row section">
									<div class="col s12 m12 l12">
										<label for="iamge">Staff Image</label>
										<img src="<?php echo base_url(); ?>assets/uploads/<?php echo $staff['image']; ?>">
										<input type="file" id="input-file-now" class="dropify" data-default-file="" name="userfile" accept="image/*" />
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col s12">
						<div class="input-field col s6">
							<div class="row">
								<div class="col s12">
									<label for="number">Phone Number</label>
								</div>
								<div class="col s12">
									<input id="number" type="number" name="number" value="<?php echo $staff['number']; ?>">
								</div>
							</div>
						</div>
						<div class="input-field col s6">
							<div class="row">
								<div class="col s12">
									<label for="username2">User Name</label>
								</div>
								<div class="col s12">
									<input id="username2" type="text" name="username" value="<?php echo $staff['username']; ?>">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col s12">
						<div class="input-field col s6">
							<div class="row">
								<div class="col s12">
									<label for="cnic">Cnic</label>
								</div>
								<div class="col s12">
									<input id="cnic" type="number" name="cnic" value="<?php echo $staff['cnic']; ?>">
								</div>
							</div>
						</div>
						<div class="input-field col s6">
							<div class="col s12">
								<label for="cnic">Role</label>
							</div>
							<select class="form-control browser-default" name="role">
                        <?php 
                           if($staff['role'] == 1)
                           {
                        ?>
                              <option selected value="1">Manager</option>
                              <option value="2">User</option>
                        <?php
                           }
                           elseif($staff['role'] == 2)
                           {
                        ?> 
                              <option value="1">Manager</option>
                              <option selected value="2">User</option>
                        <?php
                           }
                           else
                           {
                        ?>
                              <option value="" disabled selected>select Role</option>
                              <option value="1">Manager</option>
                              <option value="2">User</option>
                        <?php
                           }
                        ?>
                      
							</select>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col s12">
						<div class="input-field col s6">
							<div class="row">
								<div class="col s12">
									<label for="salary">salary</label>
								</div>
								<div class="col s12">
									<input id="salary" type="number" name="salary" value="<?php echo $staff['salary']; ?>">
								</div>
							</div>
						</div>
						<div class="input-field col s6">
							<div class="row">
								<div class="col s12">
									<label for="Fingerno">Finger Print no</label>
								</div>
								<div class="col s12">
									<input id="Fingerno" type="number" name="Fingerno" value="<?php echo $staff['Fingerno']; ?>">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col s12">
						<div class="input-field col s12">
							<button class="waves-effect waves-light  btn submit box-shadow-none border-round mr-1 mb-1 right" type="submit" name="action">submit
							<i class="material-icons right">send</i>
							</button>
						</div>
					</div>
				</div>
			</div>
			<?php echo form_close(); ?>
		</div>
	</div>
</div>