<div class="row">
	<div class="col s12">
		<div class="card br-1">
			<div class="card-content">
				<h4 class="card-title">View blogs</h4>
				<div class="row">
					<div class="col s2 right">
						<h6>Date</h6>
						<p><?php echo $blogs['blogsdate']; ?></p>
					</div>
				</div>
				<div class="row">
					<div class="col s12">
						<div class="row">
							<div class="col s6">
								<div class="input-field col s12">
									<h6 class="m-0 left">blogs Title</h6></br>
									<p>
										<?php echo $blogs['blogstitle']; ?>
									</p>
								</div>
								<div class="input-field col s12">
									<h6 class="m-0 left">Blogs Image</h6></br>
									<img src="<?php echo base_url(); ?>assets/uploads/<?php echo $blogs['blogsimg']; ?>" alt=""></a>
								</div>
							</div>
							<div class="col s6">
								<div class="input-field col s12">
									<h6 class="m-0 left">blogs</h6></br>
									<p> <?php echo $blogs['blogs']; ?></p>
									<!-- <textarea cols="30" rows="10" placeholder="Type blogs" class="blog" id="blogs" type="text" name="blogs" readonly> <?php echo $blogs['blogs']; ?></textarea> -->
								</div>
							</div>
						</div>

						<a class="waves-effect waves-light  btn  delete box-shadow-none border-round mr-1 mb-1" id="<?php echo $blogs['blogs_id']; ?>" onclick="loadblogsedit(this.id)" type="submit" name="action">Edit
							<i class="material-icons left">edit</i>
						</a>
						<a class="waves-effect waves-light  btn  delete box-shadow-none border-round mr-1 mb-1" href="<?php echo base_url(); ?>admin/deleteblogs/<?php echo $blogs['blogs_id']; ?>" type="submit" name="action">DELETE
							<i class="material-icons left">delete_forever</i>
						</a>


					</div>
				</div>
			</div>
		</div>
	</div>
</div>
