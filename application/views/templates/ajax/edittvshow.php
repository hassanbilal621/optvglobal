
<h4 class="normalheading">Edit tvshow</h4>
<?php echo form_open_multipart('admin/updatetvshow'); ?>
<div class="row">
	<div class="col s12">
		<div class="row">
			<div class="col s6">
				<div class="input-field col s12">
					<h6>tvshow Title</h6>
					<input type="text" name="tvshowtitle" value="<?php echo $tvshow['tvshowtitle']; ?>">
					<input type="hidden" value="<?php echo $tvshow['tvshowid']; ?>" name="tvshowid">
				</div>
				<div class="input-field col s12">
					<h6>tvshow Youtube Id</h6>
					<input type="text" name="tvshowurl" value="<?php echo $tvshow['tvshowurl']; ?>">
				</div>
			</div>
			<div class="col s6">
				<div class="input-field col s12">
					<h6>tvshow Thumnail</h6>
					<input type="file" id="input-file-now" class="dropify" data-default-file="" name="userfile" accept="image/*" />
				</div>
			</div>
			<div class="input-field col s12">
				<h6 for="discruption"> tvshow Discruption</h6>
				<textarea id="discruption" type="text" name="tvshowdiscruption" maxlength="500" placeholder="Type Discription 1-500" class="discrip"><?php echo $tvshow['tvshowdiscruption']; ?> </textarea>
			</div>
		</div>
		<div class="row">
			<div class="col s12">
				<div class="input-field col s12">
					<button class="waves-effect waves-light  btn submit box-shadow-none border-round mr-1 mb-1 right" type="submit" name="action">Save
						<i class="material-icons right">save</i>
					</button>
				</div>
			</div>
		</div>
	</div>
</div>
<?php echo form_close(); ?>