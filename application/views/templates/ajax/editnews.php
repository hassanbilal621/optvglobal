<h4 class="normalheading">Edit News</h4>
<?php echo form_open_multipart('admin/updatenews'); ?>
<div class="row">
	<div class="col s12">
		<div class="row">
			<div class="col s6">
				<div class="input-field col s12">
					<h6>News Title</h6>
					<input type="text" name="newstitle" value="<?php echo $news['newstitle']; ?>">
					<input type="hidden" value="<?php echo $news['newsid']; ?>" name="newsid">
				</div>
				<div class="input-field col s12">
					<h6>News Youtube Id</h6>
					<input type="text" name="newsurl" value="<?php echo $news['newsurl']; ?>">
				</div>
			</div>
			<div class="col s6">
				<div class="input-field col s12">
					<h6>News Thumnail</h6>
					<input type="file" id="input-file-now" class="dropify" data-default-file="" name="userfile" accept="image/*" />
				</div>
			</div>
			<div class="input-field col s12">
				<h6 for="discruption"> News Discruption</h6>
				<textarea id="discruption" type="text" name="newsdiscruption" maxlength="500" placeholder="Type Discription 1-500" class="discrip"><?php echo $news['newsdiscruption']; ?> </textarea>
			</div>
		</div>
		<div class="row">
			<div class="col s12">
				<div class="input-field col s12">
					<button class="waves-effect waves-light  btn submit box-shadow-none border-round mr-1 mb-1 right" type="submit" name="action">Save
						<i class="material-icons right">save</i>
					</button>
				</div>
			</div>
		</div>
	</div>
</div>
<?php echo form_close(); ?>