<div class="row">
	<div class="col s12">
		<div class="card br-1">
			<div class="card-content">
				<h4 class="card-title">Edit blogs</h4>
				<?php echo form_open_multipart('admin/updateblogs'); ?>
				<div class="row">
					<div class="col s2 right">
						<h6>Date</h6>
						<input type="text" class="datepicker" name="blogsdate" placeholder=" Type Date" required value="<?php echo $blogs['blogsdate']; ?>">
					</div>
				</div>
				<div class="row">
					<div class="col s12">
						<div class="row">
							<div class="col s6">
								<div class="input-field col s12">
									<h6>blogs Title</h6>
									<input id="blogstitle" type="text" name="blogstitle" value="<?php echo $blogs['blogstitle']; ?>">
									<input type="hidden" name="blogsid" value="<?php echo $blogs['blogs_id']; ?>">
								</div>
								<div class="input-field col s12">
								<h6>Blogs Image</h6>
								<input type="file" id="input-file-now" class="dropify" data-default-file="" name="userfile" accept="image/*" />
							
								</div>
							</div>
							<div class="col s6">
								<div class="input-field col s12">
									<h6>blogs</h6>
									<textarea cols="30" rows="10" placeholder="Type blogs" class="discrip" id="blogs" type="text" name="blogs"> <?php echo $blogs['blogs']; ?></textarea>
								</div>
							</div>
						</div>
						
						<div class="row">
							<div class="col s12">
								<div class="input-field col s12">
									<button class="waves-effect waves-light  btn submit box-shadow-none border-round mr-1 mb-1 right" type="submit" name="action">blogs Post
										<i class="material-icons right">send</i>
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>
</div>