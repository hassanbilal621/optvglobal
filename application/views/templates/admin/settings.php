<div id="main">
   <div class="row">
      <div class="col s12">
         <div class="card br-1">
            <div class="card-content">
               <h4 class="card-title">Slider</h4>
               <div class="carousel">
                     <?php foreach ($imges as $img) : ?>
                        <a class="carousel-item" href="#"><img src="<?php echo base_url(); ?>assets/uploads/<?php echo $img['image']; ?>" alt=""></a>
                        <h6><?php echo $img['discription']?></h6>
                     <?php endforeach; ?>
               </div>

               <?php echo form_open_multipart('admin/updateimage01'); ?>
               <div class="row">
                  <div class="col s12">
                     <input type="file" id="input-file-now" class="dropify" data-default-file="" name="userfile" accept="image/*" />
                     <!-- <input type="text" name="discription" class="mt-1" placeholder="Type Image Description"?>"> -->
                     <button class="waves-effect waves-light  btn submit box-shadow-none border-round mr-1 mt-1 mb-1 right" type="submit" name="action">upload Image 01
                        <i class="material-icons right">file_upload</i>
                     </button>
                  </div>
               </div>
               <?php echo form_close(); ?>
               <?php echo form_open_multipart('admin/updateimage02'); ?>
               <div class="row">
                  <div class="col s12">
                     <input type="file" id="input-file-now" class="dropify" data-default-file="" name="userfile" accept="image/*" />
                     <!-- <input type="text" name="discription" class="mt-1" placeholder="Type Image Description "> -->
                     <button class="waves-effect waves-light  btn submit box-shadow-none border-round mr-1  mt-1 mb-1 right" type="submit" name="action">upload Image 02
                        <i class="material-icons right">file_upload</i>
                     </button>
                  </div>
               </div>
               <?php echo form_close(); ?>
               <?php echo form_open_multipart('admin/updateimage03'); ?>
               <div class="row">
                  <div class="col s12">
                     <input type="file" id="input-file-now" class="dropify" data-default-file="" name="userfile" accept="image/*" />
                     <!-- <input type="text" name="discription" class="mt-1" placeholder="Type Image Description "> -->
                     <button class="waves-effect waves-light  btn submit box-shadow-none border-round mr-1 mb-1  mt-1 right" type="submit" name="action">upload Image 03
                        <i class="material-icons right">file_upload</i>
                     </button>
                  </div>
               </div>
               <?php echo form_close(); ?>
            </div>
         </div>
      </div>
   </div>
</div>

<script>
   $('.carousel').carousel({
      fullWidth: true,
      indicators: true
   });
</script>