<div id="main">
   <div class="row">
      <div class="col s12">
         <div class="card br-1">
            <div class="card-content">
               <h4 class="card-title">Home Page</h4>
               <?php echo form_open_multipart('admin/addnews'); ?>
               <div class="row">
                  <div class="col s2 right">
                     <h6>Date</h6>
                     <input type="text" class="datepicker" name="newsdate" placeholder=" Type Date" required>
                  </div>
               </div>
               <div class="row">
                  <div class="col s12">
                     <div class="row">
                        <div class="col s6">
                           <div class="input-field col s12">
                              <h6>News Title</h6>
                              <input type="text" name="newstitle" placeholder="Type News Title">
                           </div>
                           <div class="input-field col s12">
                              <h6>News Youtube Id</h6>
                              <input type="text" name="newsurl" placeholder="Type News Youtube Id">
                           </div>
                        </div>
                        <div class="col s6">
                           <div class="input-field col s12">
                              <h6>News Thumnail</h6>
                              <input type="file" id="input-file-now" class="dropify" data-default-file="" name="userfile" accept="image/*" />
                           </div>
                        </div>
                        <div class="input-field col s12">
                           <h6 for="discruption"> News Discruption</h6>
                           <textarea id="discruption" type="text" name="newsdiscruption" maxlength="500" placeholder="Type Discription 1-500" class="discrip"> </textarea>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col s12">
                           <div class="input-field col s12">
                              <button class="waves-effect waves-light  btn submit box-shadow-none border-round mr-1 mb-1 right" type="submit" name="action">Save
                                 <i class="material-icons right">save</i>
                              </button>
                           </div>
                        </div>
                     </div>
                  </div>
                  <?php echo form_close(); ?>
                  <div class="row">
                     <div class="col s12">
                        <h5 class="normalheading">Manage News</h5>
                     </div>
                  </div>
                  <div class="row" style="padding: 0 40px 0 20px;">
                     <table id="page-length-option" class="display">
                        <thead>
                           <tr>
                              <th>News Id</th>
                              <th>News Title</th>
                              <th>News Thumnail</th>
                              <th>News URL</th>
                              <th>News Discruption</th>
                              <th>Status</th>
                              <th>Action</th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php foreach ($newses as $news) : ?>
                              <tr>
                                 <td><?php echo $news['newsid']; ?></td>
                                 <td><?php echo $news['newstitle']; ?></td>
                                 <td><img src="<?php echo base_url(); ?>assets/uploads/<?php echo $news['newsthumbnail']; ?>" width="64px" /> </td>
                                 <td>
                                    <iframe src="https://www.youtube.com/embed/<?php echo  $news['newsurl']; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="width: 100%;height: 100%;"></iframe>
                                 </td>
                                 <td style="width: 15% !important;"><?php echo $news['newsdiscruption']; ?></td>
                                 <td><?php echo $news['status']; ?></td>
                                 <td>
                                    <?php
                                       if ($news['status'] == 'enable') {
                                          ?>
                                       <a class="waves-effect waves-light  btn delete box-shadow-none border-round mr-1 mb-1" onclick="disable()" href="<?php echo base_url(); ?>admin/disable/<?php echo $news['newsid']; ?>" type="submit" name="action">Disable News
                                          <i class="material-icons left">cancel</i>
                                       </a>
                                    <?php
                                       } else {
                                          ?>
                                       <a class="waves-effect waves-light  btn submit-1 box-shadow-none border-round mr-1 mb-1" onclick="enable()" href="<?php echo base_url(); ?>admin/enable/<?php echo $news['newsid']; ?>" type="submit" name="action">Enable News
                                          <i class="material-icons left">done</i>
                                       </a>
                                    <?php
                                       }
                                       ?>
                                    <button class="waves-effect waves-light  btn edit box-shadow-none border-round mr-1 mb-1 modal-trigger" onclick="loadnewsinfo(this.id)" id="<?php echo $news['newsid']; ?>" type="submit" href="#modal3" name="action">EDIT
                                       <i class="material-icons left">edit</i>
                                    </button>
                                 </td>
                              </tr>
                           <?php endforeach; ?>
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<script>
   $(document).ready(function() {
      $('.datepicker').datepicker();
   });
</script>


<script>
   function disable() {

      swal({
         title: 'Your News Now Disable',
         icon: 'error',
         buttons: false
      });
   }
</script>

<script>
   function enable() {

      swal({
         title: 'Your News Now Enable',
         icon: 'success',
         buttons: false
      });
   }
</script>



<div id="modal3" class="modal">
   <div class="modal-content">
   </div>
</div>
<script src="<?php echo base_url(); ?>assets/js/jquerynew.min.js" type="text/javascript"></script>
<script>
   function loadnewsinfo(newsid) {
      // var userid = this.id;
      $.ajax({
         type: "GET",
         url: "<?php echo base_url(); ?>admin/ajax_edit_news_adminmodal/" + newsid,
         success: function(data) {
            $(".modal-content").html(data);
            $('#modal3').modal('open');
         }
      });
   }
</script>