<div id="main">
	<div class="row">
		<div class="col s12">
			<!-- live link -->
			<div class="row">
				<div class="col s12">
					<h6 class="deep-purple-text text-darken-3 mt-5"><a href="#"><?php echo $live['liveurltitle']; ?></a></h6>
					<iframe src="https://www.youtube.com/embed/<?php echo $live['liveurl']; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="width: 100%;height: 500px;"></iframe>
				</div>
			</div>
			<!-- new -->
			<div class="row">
				<?php foreach ($newses as $news) : ?>
					<div class="col s12 m6 l6">
						<div class="card horizontal border-radius-6">
							<div class="card-image">
								<iframe width="789" class="responsive-img image-n-margin " src="https://www.youtube.com/embed/<?php echo $news['newsurl'];  ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="width: 100%;height: 300px;"></iframe>
							</div>
							<div class="card-stacked">
								<div class="card-content pl-7 pt-7 pr-7 pb-7">
									<h6 class="deep-purple-text text-darken-3 mt-5"><a href="#"><?php echo $news['newstitle']; ?></a></h6>
									<p class="mb-4"><?php echo $news['newsdiscruption']; ?></p>
								</div>
								<div class="card-action pt-4 pb-3">
									<div class="row social-icon">
										<span class="pt-2">
											<?php
												if ($news['status'] == 'enable') {
													?>
												<p>This Is <span style="color: #14ea14;font-size: large;">Enable</span> For Web</p>

												<a class="waves-effect waves-light  btn delete box-shadow-none border-round mr-1 mb-1" onclick="disable()" href="<?php echo base_url(); ?>admin/disable/<?php echo $news['newsid']; ?>" type="submit" name="action">Disable News
													<i class="material-icons left">cancel</i>
												</a>
											<?php
												} else {
													?>
												<p>This Is <span style="color: red;font-size: large;">Disable</span> For Web</p>

												<a class="waves-effect waves-light  btn submit-1 box-shadow-none border-round mr-1 mb-1" onclick="enable()" href="<?php echo base_url(); ?>admin/enable/<?php echo $news['newsid']; ?>" type="submit" name="action">Enable News
													<i class="material-icons left">done</i>
												</a>
											<?php
												}
												?>
										</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
</div>
</div>