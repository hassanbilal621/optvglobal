<div id="main">
	<div class="row">
		<div class="col s12">
			<div class="card br-1">
				<div class="card-content">
					<h4 class="card-title">Lastest blogs</h4>
					<?php echo form_open_multipart('admin/addblogs'); ?>
					<div class="row">
						<div class="col s2 right">
							<h6>Date</h6>
							<input type="text" class="datepicker" name="latestblogsdate" placeholder=" Type Date" required>
						</div>
					</div>
					<div class="row">
						<div class="col s12">
							<div class="row">
								<div class="col s6">
									<div class="input-field col s12">
										<h6>blogs Title</h6>
										<input id="blogstitle" type="text" name="blogstitle" placeholder="Type Your blogs Title">
										<h6>Live Thumnail</h6>
										<input type="file" id="input-file-now" class="dropify" data-default-file="" name="userfile" accept="image/*" />
									</div>
								</div>
								<div class="col s6">
									<div class="input-field col s12">
										<h6>blogs</h6>
										<textarea cols="30" rows="10" placeholder="Type blogs" class="blog" id="blogs" type="text" name="blogs"></textarea>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col s12">
									<div class="input-field col s12">
										<button class="waves-effect waves-light  btn submit box-shadow-none border-round mr-1 mb-1 right" type="submit" name="action">blogs Post
											<i class="material-icons right">send</i>
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php echo form_close(); ?>

					<h5 class="normalheading">Your Blogs</h5>
					<div id="search" class="search card z-depth-0 pb-5 center-align">
						<div class="card-content">
							<div class="input-field col s12">
								<h5 class="mb-2">Search Your Blogs</h5>
								<input placeholder="Search" id="searchblog" type="text" class="search-box validate white search-circle">
							</div>
						</div>
					</div>

					<?php foreach ($blogses as $blogs) : ?>

						<div id="blogs" class="col s12 m6 l4" style="min-height: 454px;">
							<div id="<?php echo $blogs['blogs_id']; ?>" onclick="loadblogsview(this.id)" class="card-panel border-radius-6 mt-10 card-animation-1">
								<a href="#"><img class="responsive-img border-radius-8 z-depth-4 image-n-margin" src="<?php echo base_url(); ?>assets/uploads/<?php echo $blogs['blogsimg']; ?>" alt=""></a>
								<h6 class="deep-purple-text text-darken-3 mt-5"><?php echo $blogs['blogstitle']; ?></h6>
								<span><?php echo substr($blogs['blogs'], 0, 50) ?>.</span>

								<div class="row mt-10">
									<div class="card-action pt-4 pb-3">
										<div class="row social-icon">
											<span class="pt-2"><?php echo $blogs['blogsdate']; ?></span>

										</div>
									</div>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	</div>
</div>
</div>

<script>
	$(document).ready(function() {
		$('.datepicker').datepicker();
	});
</script>

<div id="modal3" class="modal">
	<div class="modal-content">
	</div>
</div>
<script src="<?php echo base_url(); ?>assets/js/jquerynew.min.js" type="text/javascript"></script>
<script>
	function loadblogsedit(blogsid) {
		// var userid = this.id;
		$.ajax({
			type: "GET",
			url: "<?php echo base_url(); ?>admin/ajax_edit_blogs_adminmodal/" + blogsid,
			success: function(data) {
				$(".modal-content").html(data);
				$('#modal3').modal('open');
			}
		});
	}
</script>
<div id="modal1" class="modal">
	<div class="modal-content">
	</div>
</div>
<script>
	function loadblogsview(blogsid) {
		// var userid = this.id;
		$.ajax({
			type: "GET",
			url: "<?php echo base_url(); ?>admin/ajax_view_blogs_adminmodal/" + blogsid,
			success: function(data) {
				$(".modal-content").html(data);
				$('#modal1').modal('open');
			}
		});
	}
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
	$(document).ready(function() {
		$("#searchblog").on("keyup", function() {
			var value = $(this).val().toLowerCase();
			$("#blogs *").filter(function() {
				$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
			});
		});
	});
</script>