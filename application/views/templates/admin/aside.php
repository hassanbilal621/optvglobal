<!-- BEGIN: SideNav-->
<aside class="sidenav-main nav-expanded nav-lock nav-collapsible sidenav-light sidenav-active-square">



	<!----/////////////////////////////////////////-------------logo-----------///////////////////////////////////////// -->

	<div class="brand-sidebar">
		<h1 class="logo-wrapper">
			<a class="brand-logo darken-1" href="<?php echo base_url(); ?>admin/">
				<img src="<?php echo base_url(); ?>/assets/app-assets/images/logo/logo.png" alt="Optv Globle" style="height: 60px;margin: -20px 0 0 70px;">
			</a>
			<a class="navbar-toggler" href="#">
				<i class="material-icons">radio_button_checked</i>
			</a>
		</h1>
	</div>

	<ul class="sidenav sidenav-collapsible leftside-navigation collapsible sidenav-fixed menu-shadow" id="slide-out" data-menu="menu-navigation" data-collapsible="menu-accordion">

<!----/////////////////////////////////////////-------------Dashboard-----------///////////////////////////////////////// -->

		<li class="navigation-header">
			<a class="navigation-header-text">Dashboard</a>
		</li>
		<li class="bold">
			<a class="waves-effect waves-cyan " href="<?php echo base_url(); ?>admin/"><img src="<?php echo base_url(); ?>/assets/app-assets/images/icon/dashboard.png" alt="" style="height: 30px;margin: 0px 32px -11px 0;"><span class="menu-title red1" data-i18n="">Dashboard</span></a>
		</li>

<!----/////////////////////////////////////////-------------Staff Management-----------///////////////////////////////////////// -->

		<li class="navigation-header">
			<a class="navigation-header-text"> Staff Management</a>
			<i class="navigation-header-icon red1 material-icons">more_horiz</i>
		</li>
		<li class="bold">
			<a class="collapsible-header waves-effect waves-cyan " href="#">
				<img src="<?php echo base_url(); ?>/assets/app-assets/images/icon/staff.png" alt="" style="height: 31px;margin: 0 20px -8px 4px;"><span class="menu-title red1" data-i18n="">Staff</span></a>
			<div class="collapsible-body">
				<ul class="collapsible collapsible-sub" data-collapsible="accordion">
					<li><a class="collapsible-body" href="<?php echo base_url(); ?>admin/addstaff" data-i18n=""><i class="material-icons">keyboard_arrow_right</i><span class="red1">Add Staff</span></a></li>
					<li><a class="collapsible-body" href="<?php echo base_url(); ?>admin/managestaff" data-i18n=""><i class="material-icons">keyboard_arrow_right</i><span class="red1">Manage Staff</span></a></li>
				</ul>
			</div>
		</li>

<!----/////////////////////////////////////////-------------News Page-----------///////////////////////////////////////// -->

		<li class="navigation-header">
			<a class="navigation-header-text">News</a>
		</li>
		<li class="bold">
			<a class="waves-effect waves-cyan " href="<?php echo base_url(); ?>admin/news"><img src="<?php echo base_url(); ?>/assets/app-assets/images/icon/news.png" alt="" style="height: 30px;margin: 0px 26px -11px 0;"><span class="menu-title red1" data-i18n="">News</span></a>
		</li>

<!----/////////////////////////////////////////-------------Live Page-----------///////////////////////////////////////// -->

		<li class="navigation-header">
			<a class="navigation-header-text">Live Page</a>
		</li>
		<li class="bold">
			<a class="waves-effect waves-cyan " href="<?php echo base_url(); ?>admin/livepage"><img src="<?php echo base_url(); ?>/assets/app-assets/images/icon/live.png" alt="" style="height: 30px;margin: 0px 12px -8px -8px;"><span class="menu-title red1" data-i18n="">Live Page</span></a>
		</li>
<!----/////////////////////////////////////////-------------TV Shows-----------///////////////////////////////////////// -->

		<li class="navigation-header">
			<a class="navigation-header-text">TV Shows</a>
		</li>
		<li class="bold">
			<a class="waves-effect waves-cyan " href="<?php echo base_url(); ?>admin/tvshow"><img src="<?php echo base_url(); ?>/assets/app-assets/images/icon/tvshow.png" alt="" style="height: 30px;margin: 0px 26px -11px 0;"><span class="menu-title red1" data-i18n="">TV Shows</span></a>
		</li>

<!----/////////////////////////////////////////-------------Blogs-----------///////////////////////////////////////// -->

		<li class="navigation-header">
			<a class="navigation-header-text">Blogs</a>
		</li>
		<li class="bold">
			<a class="waves-effect waves-cyan " href="<?php echo base_url(); ?>admin/blogs"><img src="<?php echo base_url(); ?>/assets/app-assets/images/icon/blog.png" alt="" style="height: 30px;margin: 0px 25px -11px 0;"><span class="menu-title red1" data-i18n="">Blogs</span></a>
		</li>
<!----/////////////////////////////////////////-------------About Us----------///////////////////////////////////////// -->

		<li class="navigation-header">
			<a class="navigation-header-text">About Us</a>
		</li>
		<li class="bold">
			<a class="waves-effect waves-cyan " href="<?php echo base_url(); ?>admin/aboutus"><img src="<?php echo base_url(); ?>/assets/app-assets/images/icon/aboutus.png" alt="" style="height: 30px;margin: 0px 25px -11px 0;"><span class="menu-title red1" data-i18n="">About Us</span></a>
		</li>

<!----/////////////////////////////////////////-------------About Us----------///////////////////////////////////////// -->

		<li class="navigation-header">
			<a class="navigation-header-text">settings</a>
		</li>
		<li class="bold">
			<a class="waves-effect waves-cyan " href="<?php echo base_url(); ?>admin/settings"><img src="<?php echo base_url(); ?>/assets/app-assets/images/icon/setting.png" alt="" style="height: 30px;margin: 0px 25px -11px 0;"><span class="menu-title red1" data-i18n="">settings</span></a>
		</li>

<!----/////////////////////////////////////////-------------sign Out-----------///////////////////////////////////////// -->

		<li class="navigation-header"><a class="navigation-header-text" style="color: black;">Sign Out</a><i class="navigation-header-icon material-icons">more_horiz</i>
		</li>

		<li class="bold">
			<a class="waves-effect waves-cyan" href="<?php echo base_url(); ?>admin/logout"><img src="<?php echo base_url(); ?>/assets/app-assets/images/icon/logout.png" alt="" style="height: 30px;margin: 8px 15px -5px 0px;"><span class="menu-title red1" data-i18n="">Sign Out</span></a></li>


	</ul>
	<div class="navigation-background"></div>
	<a class="sidenav-trigger btn-sidenav-toggle btn-floating btn-medium waves-effect waves-light hide-on-large-only" href="#" data-target="slide-out"><i class="material-icons">menu</i></a>
</aside>
<!-- END: SideNav-->