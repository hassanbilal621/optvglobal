<?php
class Admin_model extends CI_Model
{

	public function login($username, $password)
	{

		$this->db->where('username', $username);
		$result = $this->db->get('admin');

		if ($result->num_rows() == 1) {
			$hash = $result->row(0)->password;

			if (password_verify($password, $hash)) {
				return $result->row(0)->id;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}


	//staff / Add / Update / Delete  start

	public function addstaff($imgname, $enc_password)
	{
		$data = array(
			'firstname' => $this->input->post('firstname'),
			'lastname' => $this->input->post('lastname'),
			'image' => $imgname,
			'username' => $this->input->post('username'),
			'password' => $enc_password,
			'cnic' => $this->input->post('cnic'),
			'number' => $this->input->post('number'),
			'salary' => $this->input->post('salary'),
			'Fingerno' => $this->input->post('Fingerno'),
			'role' => $this->input->post('role')
		);

		$this->security->xss_clean($data);
		$this->db->insert('staff', $data);
	}

	public function get_staff()
	{



		$query = $this->db->get('staff');
		$this->db->order_by('staff.staff_id', 'desc');
		return $query->result_array();
	}

	public function update_staff($imgname,  $staffid)
	{
		$data = array(
			'firstname' => $this->input->post('firstname'),
			'lastname' => $this->input->post('lastname'),
			'image' => $imgname,
			'username' => $this->input->post('username'),
			'cnic' => $this->input->post('cnic'),
			'number' => $this->input->post('number'),
			'salary' => $this->input->post('salary'),
			'Fingerno' => $this->input->post('Fingerno'),
			'role' => $this->input->post('role')
		);

		$this->security->xss_clean($data);
		$this->db->where('staff_id', $staffid);
		$this->db->update('staff', $data);
	}

	public function del_staff($staffid)
	{
		$this->db->where('staff_id', $staffid);
		$this->db->delete('staff');
	}


	//staff / Add / Update / Delete  End

	//news / Add / Update / Delete  start

	public function addnews($imgname)
	{
		$data = array(
			'newstitle' => $this->input->post('newstitle'),
			'newsurl' => $this->input->post('newsurl'),
			'newsdiscruption' => $this->input->post('newsdiscruption'),
			'newsthumbnail'	=>$imgname,
			'postdate' => date('Y-m-d H:i:s'),
			'newsdate' => $this->input->post('newsdate'),
			'status' => 'disable'
		);

		$this->security->xss_clean($data);
		$this->db->insert('news', $data);
	}

	public function get_news()
	{
		$this->db->order_by('news.newsid', 'desc');
		$query = $this->db->get('news');
		return $query->result_array();
	}

	public function get_news_by_id($newsid)
	{
		$this->db->where('newsid', $newsid);
		$query = $this->db->get('news');
		return $query->result_array();
	}

	public function get_news_limit()
	{
		$this->db->order_by('news.newsid', 'desc');
		$this->db->limit(10);
		$query = $this->db->get('news');

		return $query->result_array();
	}

	public function update_news($imgname, $newsid)
	{
		$data = array(
			'newstitle' => $this->input->post('newstitle'),
			'newsurl' => $this->input->post('newsurl'),
			'newsdiscruption' => $this->input->post('newsdiscruption'),
			'newsthumbnail'	=>$imgname,
			'postdate' => date('Y-m-d H:i:s'),
			'newsdate' => $this->input->post('newsdate'),
		);

		$this->security->xss_clean($data);
		$this->db->where('newsid', $newsid);
		$this->db->update('news', $data);
	}

	public function disable($newsid)
	{
		$data = array(
			'status' => 'disable'
		);

		$this->security->xss_clean($data);
		$this->db->where('newsid', $newsid);
		$this->db->update('news', $data);
	}

	public function enable($newsid)
	{
		$data = array(
			'status' => 'enable'
		);

		$this->security->xss_clean($data);
		$this->db->where('newsid', $newsid);
		$this->db->update('news', $data);
	}
	public function offline($liveid)
	{
		$data = array(
			'status' => 'offline'
		);

		$this->security->xss_clean($data);
		$this->db->where('live_id', $liveid);
		$this->db->update('livepage', $data);
	}

	public function online($liveid)
	{
		$data = array(
			'status' => 'online'
		);

		$this->security->xss_clean($data);
		$this->db->where('live_id', $liveid);
		$this->db->update('livepage', $data);
	}

	//news / Add / Update / Delete  End

	//tvshow / Add / Update / Delete  start

	public function addtvshow($imgname)
	{
		$data = array(
			'tvshowtitle' => $this->input->post('tvshowtitle'),
			'tvshowurl' => $this->input->post('tvshowurl'),
			'tvshowdiscruption' => $this->input->post('tvshowdiscruption'),
			'tvshowthumbnail'	=>$imgname,
			'postdate' => date('Y-m-d H:i:s'),
			'tvshowdate' => $this->input->post('tvshowdate'),
			'status' => 'disable'
		);

		$this->security->xss_clean($data);
		$this->db->insert('tvshow', $data);
	}

	public function get_tvshow()
	{
		$this->db->order_by('tvshow.tvshowid', 'desc');
		$query = $this->db->get('tvshow');
		return $query->result_array();
	}

	public function get_tvshow_limit()
	{
		$this->db->order_by('tvshow.tvshowid', 'desc');
		$this->db->limit(10);
		$query = $this->db->get('tvshow');

		return $query->result_array();
	}

	public function update_tvshow($imgname, $tvshowid)
	{
		$data = array(
			'tvshowtitle' => $this->input->post('tvshowtitle'),
			'tvshowurl' => $this->input->post('tvshowurl'),
			'tvshowdiscruption' => $this->input->post('tvshowdiscruption'),
			'tvshowthumbnail'	=>$imgname,
			'postdate' => date('Y-m-d H:i:s'),
			'tvshowdate' => $this->input->post('tvshowdate'),
		);

		$this->security->xss_clean($data);
		$this->db->where('tvshowid', $tvshowid);
		$this->db->update('tvshow', $data);
	}

	public function disabletvshow($tvshowid)
	{
		$data = array(
			'status' => 'disable'
		);

		$this->security->xss_clean($data);
		$this->db->where('tvshowid', $tvshowid);
		$this->db->update('tvshow', $data);
	}

	public function enabletvshow($tvshowid)
	{
		$data = array(
			'status' => 'enable'
		);

		$this->security->xss_clean($data);
		$this->db->where('tvshowid', $tvshowid);
		$this->db->update('tvshow', $data);
	}


	//tvshow / Add / Update / Delete  End

	public function addlivepage($imgname, $livepageid)
	{
		$data = array(
			'liveurltitle' => $this->input->post('liveurltitle'),
			'liveurl' => $this->input->post('liveurl'),
			'thumnail' => $imgname,
			'liveurldiscruption' => $this->input->post('liveurldiscruption'),
			'postdate' => date('Y-m-d H:i:s'),
			'livedate' => $this->input->post('livedate'),
			'status' => 'offline'
		);

		$this->security->xss_clean($data);

		$this->db->where('live_id', $livepageid);
		$this->db->update('livepage', $data);
	}


	public function addblogs($imgname)
	{
		$data = array(
			'blogstitle' => $this->input->post('blogstitle'),
			'blogs' => $this->input->post('blogs'),
			'blogsdate' => $this->input->post('blogsdate'),
			'postdate' => date('Y-m-d H:i:s'),
			'blogsimg' => $imgname,
			'status' => 'enable'
		);

		$this->security->xss_clean($data);
		$this->db->insert('blogs', $data);
	}

	public function updateblogs($imgname, $blogsid)
	{
		$data = array(

			'blogstitle' => $this->input->post('blogstitle'),
			'blogs' => $this->input->post('blogs'),
			'blogsdate' => $this->input->post('blogsdate'),
			'postdate' => date('Y-m-d H:i:s'),
			'blogsimg' => $imgname,
			'status' => 'enable'
		);

		$this->security->xss_clean($data);
		$this->db->where('blogs_id', $blogsid);
		$this->db->update('blogs', $data);
	}

	public function deleteblogs($blogsid)
	{
		$this->db->where('blogs_id', $blogsid);
		$this->db->delete('blogs');
	}

	public function get_live_streaming()
	{
		$query = $this->db->get('livepage');
		return $query->row_array();
	}

	public function get_live($livepageid)
	{
		$this->db->where('live_id', $livepageid);
		$query = $this->db->get('livepage');
		return $query->row_array();
	}



	public function get_about()
	{
		$this->db->where('aboutid', 1);
		$query = $this->db->get('aboutus');
		return $query->row_array();
	}

	public function update_about()
	{
		$data = array(
			'name' => $this->input->post('name'),
			'contact-01' => $this->input->post('contact-01'),
			'contact-02' => $this->input->post('contact-02'),
			'aboutus' => $this->input->post('aboutus')
		);

		$this->security->xss_clean($data);
		$this->db->where('aboutid', '1');
		$this->db->update('aboutus', $data);
	}
	public function updateimage01($imgname)
	{ {
			$data = array(
				'image' => $imgname

			);

			$this->security->xss_clean($data);
			$this->db->where('slider_id', '1');
			$this->db->update('slider', $data);
		}
	}
	public function updateimage02($imgname)
	{ {
			$data = array(
				'image' => $imgname

			);

			$this->security->xss_clean($data);
			$this->db->where('slider_id', '2');
			$this->db->update('slider', $data);
		}
	}
	public function updateimage03($imgname)
	{ {
			$data = array(
				'image' => $imgname

			);

			$this->security->xss_clean($data);
			$this->db->where('slider_id', '3');
			$this->db->update('slider', $data);
		}
	}

	public function img()
	{
		$query = $this->db->get('slider');
		return $query->result_array();
	}	

	/////******************************AJAx*******************************************//////


	public function get_ajax_staff($staffid)
	{
		$this->db->where('staff_id', $staffid);
		$result = $this->db->get('staff');
		return $result->row_array();
	}


	public function get_ajax_news($newsid)
	{
		$this->db->where('newsid', $newsid);
		$result = $this->db->get('news');
		return $result->row_array();
	}
	public function get_ajax_tvshow($tvshowid)
	{
		$this->db->where('tvshowid', $tvshowid);
		$result = $this->db->get('tvshow');
		return $result->row_array();
	}

	public function get_ajax_blogs($blogsid)
	{
		$this->db->where('blogs_id', $blogsid);
		$result = $this->db->get('blogs');
		return $result->row_array();
	}


	////////////////////////////////////api

	public function get_live_streaming_api()
	{
		$query = $this->db->get('livepage');
		return $query->result_array();
	}

	public function get_aboutus_api()
	{
		$query = $this->db->get('aboutus');
		return $query->row_array();
	}

	public function get_blogs()
	{
		$query = $this->db->get('blogs');
		return $query->result_array();
	}

	public function get_news_api()
	{

		$this->db->where('status', 'enable');
		$query = $this->db->get('news');
		return $query->result_array();
	}

	public function get_tvshow_api()
	{

		$this->db->where('status', 'enable');
		$query = $this->db->get('tvshow');
		return $query->result_array();
	}

	public function get_slider()
	{

		$query = $this->db->get('slider');
		return $query->result_array();
	}
	
	public function get_news_api_limit()
	{
		$this->db->order_by('news.newsid', 'desc');
		$this->db->limit(8);
		$this->db->where('status', 'enable');
		$query = $this->db->get('news');
		return $query->result_array();
	}
}
