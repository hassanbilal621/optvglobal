<?php
class User_model extends CI_Model{
	public function __construct()
    {
      parent::__construct();
	  $this->load->database();
    }
	

    public function register($enc_password){
        $data = array(
            'name' => $this->input->post('name'),
			'email' => $this->input->post('email'),
			'phone' => $this->input->post('number'),
            'password' => $enc_password
        );

		$this->security->xss_clean($data);
        return $this->db->insert('users', $data);
	}

	public function checkout($user_id)
	{
		$data = array(

            'date_posted' => date('Y-m-d H:i:s'),
            'academic_level_id' => $this->input->post('academic_level'),
			'typesofpaper_id' => $this->input->post('typesofpaper'),
			'noofday_id' => $this->input->post('noofday'),
			'noofpage_id' => $this->input->post('noofpage'),
			'desc' => $this->input->post('desc'),
			'userid' => $user_id
        );
		
		
		$this->security->xss_clean($data);
        $this->db->insert('users_orders', $data);
	}


	

	public function get_allorders($userid){
		$this->db->order_by('users_orders.orderid', 'DESC');
		$this->db->join('academic_level', 'users_orders.academic_level_id = academic_level.academic_level_id', 'left');
		$this->db->join('typesofpaper', 'users_orders.typesofpaper_id = typesofpaper.typeofpaper_id', 'left');
		$this->db->join('noofdays', 'users_orders.noofday_id = noofdays.noofdays_id', 'left');
		$this->db->join('noofpages', 'users_orders.noofpage_id = noofpages.noofpages_id', 'left');

		$this->db->where('users_orders.userid', $userid);
        $query = $this->db->get('users_orders');
        return $query->result_array();
	}

	public function get_courses(){
        $this->db->order_by('courses.cid', 'DESC');
		$query = $this->db->get('courses');
		

		
        return $query->result_array();
	}

	public function get_cart($userid){
		$this->db->order_by('cart.cart_id', 'DESC');
		$this->db->join('courses', 'cart.product_id = courses.cid', 'left');
		$this->db->where('userid', $userid);
		$query = $this->db->get('cart');
		
        return $query->result_array();
	}

	public function delete_item($cartid)
	{
		$this->db->where('cart_id', $cartid);
		$this->db->delete('cart');
	}


	public function checkcart($courseid , $userid){
		$this->db->where('product_id', $courseid);
		$this->db->where('userid', $userid);
		$query = $this->db->get('cart');
        return $query->result_array();
	}

	public function create_invoice($userid, $totalamount){
		$data = array(
			'amount' => $totalamount,
            'invoice_date' => date('Y-m-d H:i:s'),
			'userid' => $userid
        );
		
		$this->security->xss_clean($data);
		$this->db->insert('users_invoices', $data);
		
		$insertId = $this->db->insert_id();

		return  $insertId;

	}

	public function complete_order($invoiceid){
		
		$this->db->where('invoice_id', $invoiceid);
		$data = array(
			'pay_status' => 'paid'
		);
		$this->db->update('users_invoices', $data);

	}


		
	public function add_order($courseid, $invoiceid , $userid){
		$data = array(
			'courseid' => $courseid,
			'invoiceid' => $invoiceid,
			'date_posted' => date('Y-m-d H:i:s'),
			'userid' => $userid
        );
		
		$this->security->xss_clean($data);
        $this->db->insert('users_orders', $data);

	}
	
	public function addcart($courseid , $userid){
		$data = array(
            'product_id' => $courseid,
			'userid' => $userid
        );
		
		$this->security->xss_clean($data);
        $this->db->insert('cart', $data);

	}

	public function check_courseinfo($orderid, $userid)
	{
		$this->db->order_by('users_orders.orderid', 'DESC');
		
		$this->db->join('courses', 'users_orders.courseid = courses.cid', 'left');
		$this->db->join('users_invoices', 'users_orders.invoiceid = users_invoices.invoice_id', 'left');
		$this->db->where('users_orders.userid', $userid);
		$this->db->where('orderid', $orderid);
		$this->db->where('pay_status', "paid");
		$query = $this->db->get('users_orders');

		return $query->result_array();
	}



	public function get_coursechapters($userid){
		$this->db->order_by('users_orders.orderid', 'DESC');
		$this->db->where('users_orders.userid', $userid);
		$this->db->join('courses', 'users_orders.courseid = courses.cid', 'left');
		$this->db->join('users_invoices', 'users_orders.invoiceid = users_invoices.invoice_id', 'left');
        $query = $this->db->get('users_orders');
        return $query->result_array();
	}


	public function get_invoiceorders($invoiceid){
		$this->db->order_by('users_orders.orderid', 'DESC');
		$this->db->join('courses', 'users_orders.courseid = courses.cid', 'left');
		$this->db->join('users_invoices', 'users_orders.invoiceid = users_invoices.invoice_id', 'left');
		$this->db->where('invoice_id', $invoiceid);
        $query = $this->db->get('users_orders');
        return $query->result_array();
	}

	public function addeducation($user_id)
	{
		$data = array(
            'degree' => $this->input->post('degree'),
            'desc' => $this->input->post('desc'),
            'startdate' => $this->input->post('startdate'),
            'enddate' => $this->input->post('enddate'),
			'type' => $this->input->post('type'),
			'userid' => $user_id
        );
		
		
		$this->security->xss_clean($data);
        $this->db->insert('education', $data);
	}

	public function addexperience($user_id)
	{
		$data = array(
			'tittle' => $this->input->post('tittle'),
			'company' => $this->input->post('company'),
            'desc' => $this->input->post('desc'),
            'startdate' => $this->input->post('startdate'),
            'enddate' => $this->input->post('enddate'),
			'userid' => $user_id
        );
		
		
		$this->security->xss_clean($data);
        $this->db->insert('experiences', $data);
	}

	public function addproject($user_id, $imgname)
	{

		$data = array(
			'tittle' => $this->input->post('tittle'),
            'desc' => $this->input->post('desc'),
            'date' => $this->input->post('startdate'),
            'img' => $imgname,
			'userid' => $user_id
        );
				
		$this->security->xss_clean($data);
        $this->db->insert('projects', $data);
	}

	// public function get_education(){
    //     $this->db->order_by('education.id', 'DESC');
    //     $query = $this->db->get('education');
    //     return $query->result_array();
    // }

	// public function get_projects(){
    //     $this->db->order_by('projects.id', 'DESC');
    //     $query = $this->db->get('projects');
    //     return $query->result_array();
    // }

	// public function get_experiences(){
    //     $this->db->order_by('experiences.id', 'DESC');
    //     $query = $this->db->get('experiences');
    //     return $query->result_array();
    // }

	public function add_blog_comments(){
        $data = array(
            'comment' => $this->input->post('comment'),
			'blogid' => $this->input->post('blogid'),
			'useravt' => $this->input->post('useravt'),
            'datetime' => date('Y-m-d H:i:s'),
			'user' => $this->input->post('user')
        );

		$this->security->xss_clean($data);
        return $this->db->insert('blog_comment', $data);
	}
	
    public function get_blog_all(){
        $this->db->order_by('blog.id', 'DESC');
        $query = $this->db->get('blog', 10);
        return $query->result_array();
    }
	
    public function login($username, $password){
	

		$this->db->where('email', $username);
		$result = $this->db->get('users');

	
        if($result->num_rows() == 1){
			$hash = $result->row(0)->password;
			
			if (password_verify($password, $hash))
			{
				return $result->row(0)->id;
			}
			else
			{
				return false;
			}
            
			
			
        } else {
            return false;
        }
    }
	
	/*
     * Function: check_username_exists
     * Purpose: This method checks to see if passed in username exists in database, 
				if does will return true, otherwise false
     * Params: $username - the username
     * Return: True if username exists, false if not
     */
    public function check_user_exists($username)
	{
		$this->db->where('username', $username);
		$result = $this->db->get('users');
		if($result)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public function delproject($projectid)
	{
		$this->db->where('id', $projectid);
		$this->db->delete('projects');
	}
	public function delexperience($expid)
	{
		$this->db->where('id', $expid);
		$this->db->delete('experiences');
	}
	public function deleducation($eduid)
	{
		$this->db->where('id', $eduid);
		$this->db->delete('education');
	}

	public function remove_user()
	{
		$this->db->where('user_id', $this->session->userdata('user_id'));
		$this->db->delete('jobs');
		
		$this->db->where('id', $this->session->userdata('user_id'));
		$this->db->delete('users');
	}
	
	public function set_password_reset_token($username, $token)
	{
		
		$this->db->where('username', $username);
		$data = array(
			'password_reset_token'=>$token
		);
		$this->db->update('users', $data);

	}
	
	public function check_token_exists($token)
	{

		$this->db->where('password_reset_token', $token);
		$result = $this->db->get('users');
		if($result)
		{
			return $result->row(0)->id;
		}
		else
		{
			return 0;
		}
	}
	public function change_password($user_id,$enc_password)
	{
		$this->db->where('id', $user_id);
		$data = array(
			'password'=>$enc_password,
			'password_reset_token'=>""
		);
		$this->db->update('users', $data);

		
		
	}

	public function make_session()
	{
		$user_id = get_cookie('jobboard_user_id');
		if($user_id)
		{
			$this->db->where('id', $user_id);
			$result = $this->db->get('users');
			
			$user_data = array(
				'user_id' => $user_id,
				'username' => $result->row(0)->username,
				'logged_in' => true
			);
			$this->session->set_userdata($user_data);
		}
		


	}
	
	public function get_userinfo($user_id)
	{
		$this->db->where('id', $user_id);
		$result = $this->db->get('users');
		
		return $result->row_array();
	}
	

	
    public function get_blogs($blogid)
    {

		$this->db->where('id', $blogid);
		$result = $this->db->get('blog');
		
		return $result->row_array();
	}


	public function get_blog_comments($blogid){
        $this->db->where('blogid', $blogid);
        $this->db->order_by('blog_comment.id', 'DESC');
        $query = $this->db->get('blog_comment', 10);
        return $query->result_array();
    }
	/*
     * Function: get_empname
     * Purpose: This method is responsible for returning the name of an employer( user's name)
     * Params:  $userid: the id of the user
     * Return: name of a user
     */
    public function get_empname($userid)
    {
        $query = $this->db->get_where('users', array('id'=>$userid));
        return $query->row_array()['name'];
    }
	
	public function update($user_id, $imgname)
	{
		$data = array(
            'name' => $this->input->post('name'),
            'email' => $this->input->post('email'),
            'address' => $this->input->post('address'),
            'fathername' => $this->input->post('fname'),
			'phone' => $this->input->post('phone'),
			'country' => $this->input->post('country'),
			'city' => $this->input->post('city'),
			'picture' => $imgname
        );
		
		
		$this->security->xss_clean($data);
		$this->db->where('id', $user_id);
        $this->db->update('users', $data);
	}


}


